from flask import *
import hashlib
import sqlite3
# from datetime import datetime
from random import randint
# from email.mime.text import MIMEText
# import smtplib


def sha256(a):
    sha256 = hashlib.sha256()
    sha256.update(a.encode('utf-8'))
    return sha256.hexdigest()


def register():
    conn = sqlite3.connect('SQL.sqlite3')
    cur = conn.cursor()
    username = request.form.get("username")
    password = request.form.get("password")
    cur.execute("INSERT INTO USER VALUES ('" +
                username+"', '"+sha256(password)+"');")
    conn.commit()


def login(name, password=None):
    conn = sqlite3.connect('SQL.sqlite3')
    cur = conn.cursor()
    cursor = cur.execute("SELECT * FROM USER")
    for raw in cursor:
        if raw[0] == name and password is None or raw[0] == name and raw[1] == sha256(password):
            return True
    return False


# def code_message(to_addr):
#     global used_code_time,used_code,from_addr,mail_password,smtp_server,mail_server
#     used_code.append(n)
#     Time = datetime.now()
#     if Time.year>used_code_time[0] and Time.day>used_code_time[1]:
#         used_code_time[0]=Time.year
#         used_code_time[1]=Time.day
#     while True:
#         n=randint(1000,999999)
#         if n not in used_code:
#             msg = MIMEText('你好，下方是你的网站验证码：\n'+str(n), 'plain', 'utf-8')
#             mail_server.starttls()
#             mail_server.login(from_addr, mail_password)
#             mail_server.sendmail(from_addr, [to_addr], msg.as_string())
#             mail_server.quit()


app = Flask(__name__)


app.secret_key = 'wdudhuhwhhfgyurwehedqehwqehduqhdedueiudwh'

# used_code_time=[0,0]
# used_code=[]
# from_addr = 'code_message@outlook.com'
# mail_password = 'zijun0925'
# smtp_server = 'smtp.office365.com'
# mail_server = smtplib.SMTP(smtp_server, 587)


@app.route('/', methods=['GET', 'post'])
def loading():
    if request.method == 'GET':
        if session.get("login") == "1":
            return render_template("loading.html", s=str(randint(2, 4)), u="/study/"+session.get('username')+"/")
        return render_template("loading.html", s=str(randint(2, 4)), u='/login/')


@app.route('/login/', methods=['GET', 'post'])
def login_page():
    if session.get("login") == "1":
        return redirect("/study/"+session.get('username')+"/")
    if request.method == 'GET':
        session.get('login', "0")
        return render_template("login.html")
    username = request.form.get("username")
    password = request.form.get("password")
    if login(username, password):
        session['login'] = '1'
        session['username'] = username
        return redirect("/study/"+session.get('username')+"/")
    else:
        flash("用户名或密码错误")
        return render_template("login.html")


@app.route("/register/", methods=['GET', 'post'])
def register_page():
    if request.method == 'GET':
        session.get('login', "0")
        return render_template("register.html")
    if request.form.get("username") == '' or request.form.get("password") == '':
        flash("用户名或密码不能为空")
        return render_template("register.html")
    elif login(request.form.get("username")):
        flash("用户已存在")
        return render_template("register.html")
    else:
        register()
        return redirect("/login/")


@app.route("/study/<user_name>/")
def study_page(user_name):
    if session.get("login") != "1":
        return redirect("/login/")
    if request.method == 'GET':
        session.get('login', "0")
        return render_template("study.html", user_name=user_name)


if __name__ == '__main__':
    app.run('0.0.0.0', 80, debug=True)